﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.FixtureTest;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        //TODO: Add Unit Tests
        private readonly Mock<IRepository<Partner>> _partnerRepositoryMick;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerRepositoryMick = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_For_Partner_ISNotFound_Returns_NotFound()
        {
            //Arrange
            var partnerId = Guid.Parse("56051d6d-2eee-477b-bd02-1c8a7116cb1b");
            Partner partner = null;
            _partnerRepositoryMick.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);
            
            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_For_IsActive_False_Returns_BadRequestObjectResult()
        {
            //Arrange
            Guid id = Guid.Parse("56051d6d-2eee-477b-bd02-1c8a7116cb1b");
            _partnerRepositoryMick.Setup(x => x.GetByIdAsync(id));
            
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(id, null);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_For_Success_Returns_CreatedAtActionResult()
        {
            var partnerId = Guid.Parse("571b48c2-5bcb-4e11-b31a-d6d967043ea7");
            _partnerRepositoryMick.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(GetPartners().FirstOrDefault(x => x.Id == partnerId));
            var partner = GetPartners().FirstOrDefault(x => x.Id == partnerId);


            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, GetRequest().FirstOrDefault(x => x.Limit == 2));

            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_For_Promocod_Reset_To_Zero_Returns_True()
        {
            var partnerId = Guid.Parse("571b48c2-5bcb-4e11-b31a-d6d967043ea7");
            var partner = GetPartners().FirstOrDefault(x => x.Id == partnerId);
            
            _partnerRepositoryMick.Setup(repo => repo.GetByIdAsync(partnerId))
                                  .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, GetRequest().FirstOrDefault(x => x.Limit == 2));

            Assert.Equal(0, partner.NumberIssuedPromoCodes);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_For_Promocod_Is_Not_Reset_Returns_True()
        {
            var partnerId = Guid.Parse("a55a8ffc-826e-4ae8-bc8c-8d675f0eede2");
            var partner = GetPartners().FirstOrDefault(x => x.Id == partnerId);
            _partnerRepositoryMick.Setup(repo => repo.GetByIdAsync(partnerId))
                                  .ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, GetRequest().FirstOrDefault(x => x.Limit == 2));

            Assert.Equal(2, partner.NumberIssuedPromoCodes);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_For_LimitEqualToZero_Returns_BadRequestObjectResult(int limit)
        {
            //Arrange
            var partnerId = Guid.Parse("571b48c2-5bcb-4e11-b31a-d6d967043ea7");
            _partnerRepositoryMick.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(GetPartners().FirstOrDefault(x => x.Id == partnerId));

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, GetRequest().FirstOrDefault(x => x.Limit == limit));

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_For_PartnerPromoCodeLimitNull_Returns_ArgumentNullexception()
        {
            //Arrange
            var partnerId = Guid.Parse("ad72160c-38e2-4698-b0fb-4ad0be297c46");
            _partnerRepositoryMick.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(GetPartners().FirstOrDefault(x => x.Id == partnerId));


            //Act
            var ex = await Assert.ThrowsAsync<ArgumentNullException>(() => _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, GetRequest().FirstOrDefault(x => x.Limit == 0)));

            //Aseert
            ex.Message.Should().Contain("cannot be null");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_For_UpdateAsync_Returns_True()
        {
            var partnerId = Guid.Parse("a55a8ffc-826e-4ae8-bc8c-8d675f0eede2");
            var partner = GetPartners().FirstOrDefault(x => x.Id == partnerId);
            var setPArtPromLimitRequest = GetRequest().FirstOrDefault(x => x.Limit == 2);
            _partnerRepositoryMick.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _partnerRepositoryMick.Setup(repo => repo.UpdateAsync(partner));

            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPArtPromLimitRequest);

            _partnerRepositoryMick.Verify(x => x.UpdateAsync(partner), Times.Once);
        }


        private List<SetPartnerPromoCodeLimitRequest> GetRequest()
        {
            return new List<SetPartnerPromoCodeLimitRequest>()
            {
                new SetPartnerPromoCodeLimitRequest
                {
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = 2
                },
                new SetPartnerPromoCodeLimitRequest
                {
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = 0
                },
                new SetPartnerPromoCodeLimitRequest
                {
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = -1
                }
            };
        }

        private List<Partner> GetPartners()
        {
            var partners = new List<Partner>
            {
                new Partner
                {
                    Id = Guid.Parse("571b48c2-5bcb-4e11-b31a-d6d967043ea7"),
                    IsActive = true,
                    Name = "Алабай",
                    NumberIssuedPromoCodes = 2,
                    PartnerLimits = new List<PartnerPromoCodeLimit>()
                    {
                        new PartnerPromoCodeLimit()
                        {
                            Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                            CreateDate = new DateTime(2020, 07, 9),
                            EndDate = new DateTime(2020, 10, 9),
                            Limit = 100,
                        }
                    }
                },
                new Partner
                {
                    Id = Guid.Parse("a55a8ffc-826e-4ae8-bc8c-8d675f0eede2"),
                    IsActive = true,
                    Name = "Алабай",
                    NumberIssuedPromoCodes = 2,
                    PartnerLimits = new List<PartnerPromoCodeLimit>()
                    {
                        new PartnerPromoCodeLimit()
                        {
                            Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                            CreateDate = new DateTime(2020, 07, 9),
                            EndDate = new DateTime(2020, 10, 9),
                            Limit = 100,
                            CancelDate = new DateTime(2020,11,5)
                        }
                    }
                },
                new Partner
                {
                    Id = Guid.Parse("56051d6d-2eee-477b-bd02-1c8a7116cb1b"),
                    IsActive = false,
                    Name = "Мухтар",
                    PartnerLimits = new List<PartnerPromoCodeLimit>()
                    {
                        new PartnerPromoCodeLimit()
                        {
                            Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                            CreateDate = new DateTime(2020, 07, 9),
                            EndDate = new DateTime(2020, 10, 9),
                            Limit = 100,
                        }
                    }
                },
                new Partner
                {
                    Id = Guid.Parse("ad72160c-38e2-4698-b0fb-4ad0be297c46"),
                    IsActive = true,
                    Name = "фывафыва",
                    PartnerLimits = null
                }
            };
            return partners;
        }
    }
}