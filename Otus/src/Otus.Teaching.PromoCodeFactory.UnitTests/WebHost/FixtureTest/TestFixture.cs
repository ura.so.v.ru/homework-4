﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using FluentAssertions.Common;
using Otus.Teaching.PromoCodeFactory.WebHost;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.FixtureTest
{
    class TestFixture : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }
        public TestFixture()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var configuration = configurationBuilder.Build();
            var serviceCollection = new ServiceCollection();
            new Startup(configuration).ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            ServiceProvider = serviceProvider;
        }

        public void Dispose()
        {
        }
    }
}
